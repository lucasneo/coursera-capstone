# loading libraries
library(dplyr)
library(ggplot2)
library(tm)

# setwd to the right directory and load the data
setwd("coursera-capstone/")
load("~/coursera-capstone/data-explore.RData")

########################
# EXPLORATORY ANALYSIS #
########################

#-------#
# STARS #
#-------#

# business star ratings:
# 50% of all businesses got at least 3.5 stars and the mean is 3.7
# 15.6% had 4.5 stars and 12% had 5 stars -> total of 27%
prop.table(table(biz$stars))
quantile(biz$stars,seq(0,1,0.1)) # The higest proportion of businesses get 3.5 (21.5%) or 4 stars (22%)
sum(biz$review_count) # there are 1,729,825 reviews of businesses
tbl_df(biz) %>% group_by(stars) %>% summarize(reviews = sum(review_count))
# the proportion of reviews for each star rating also follows the distribution of star ratings

#----------------#
# TIPS AND LIKES #
#----------------#
# tips:
# tips are short notes users leave about a place without having to write a full review
tip_df = tbl_df(tip_raw)

# tips and likes per business:
# number of tips and likes per business
# median tips is 3 and 90th percentile is 26. There are 4196 businesses that have more than 26.
# There is no trend between likes and tips.
biz_tips = tip_df %>% group_by(business_id) %>% summarise(tips = n(), likes = sum(likes))
summary(biz_tips$tips)
quantile(biz_tips$tips,seq(0,1,0.1))
count(biz_tips[biz_tips$tips > 26,])
qplot(biz_tips$likes, biz_tips$tips, binwidth = 1)
biz = merge(business_raw,biz_tips,by='business_id',all.x = T)

# reviews per business:
summary(biz$review_count)

# tips per user:
# summary statistics; mean 6, median 2, 3rd quantile is 4, max is 1306
# 95% of user tips are <22
user_tips = tip_df %>% group_by(user_id) %>% summarise(tips = n())
qplot(user_tips$tips, binwidth = 10, xlim = c(0,300))
quantile(user_tips$tips,seq(0,1,0.05))
count(user_tips[user_tips$tips > 22,])
user = merge(user_raw,user_tips,by='user_id',all.x=T)

# number of reviews by users with a lot of tips:
user[is.na(user$tips),]$tips = 0
quantile(user[user$tips < 22,]$review_count,seq(0,1,0.1))

#--------------#
# BIZ LOCATION
#--------------#
# city statistics:
# Las Vegas makes up 22% of all the businesses
# there are 16896 businesses (city is not NA) with more than 4 stars
# naturually, they also have the highest number of 4.5 and 5 star reviews at 2024 and 1736
city = tbl_df(biz) %>% group_by(city) %>% summarize(count = n())

city = tbl_df(biz) %>%
  select(city, stars) %>%
  filter(stars > 4) %>%
  group_by(city, stars) %>% summarize(count = n()) %>% 
  ungroup() %>% arrange(desc(count), city)

# clean the neighborhoods attribute:
biz$neighborhoods = as.character(biz$neighborhoods)
biz$neighborhoods = gsub("character\\(0\\)",NA,biz$neighborhoods)
biz$neighborhoods = gsub("list\\(\\)",NA,biz$neighborhoods)
biz$neighborhoods = gsub("c\\(\"","",biz$neighborhoods)
biz$neighborhoods = gsub("\\\"","",biz$neighborhoods)
biz$neighborhoods = gsub("\\)","",biz$neighborhoods)

# finding the mean number of stars for each neighborhood:
neighborhoods = tbl_df(biz) %>% 
  group_by(neighborhoods) %>% 
  summarize(count = n(), stars = sum(stars)) %>%
  mutate(mean = stars / count)
qplot(neighborhoods$neighborhoods, neighborhoods$mean) # no relationship

# number of 4.5 and 5 star restaurants in neighborhoods:
# westside, southeast, spring valley had the most number of 4.5 and 5 star businesses
# they were 529, 412, and 412 respectively
# these numbers may not be as significant since 64% of all businesses left this field blank
neighborhoods = tbl_df(biz) %>%
  select(stars, neighborhoods) %>%
  filter(stars > 4) %>%
  group_by(neighborhoods, stars) %>% summarize(count = n())

#-------------#
# TEXT MINING
#-------------#
library(dplyr)
library(ggplot2)
library(tm)
library(SnowballC)
library(doParallel)

# start up parallel processing
cluster = makeCluster(detectCores() - 1)
registerDoParallel(cluster)
showConnections() # this allows us to confirm that there are 15 worker connections
stopCluster(cluster)

# loading the data into a corpus
corpus = Corpus(VectorSource(reviews$text))

# to inspect the first document in the corpus, just access it like a normal list
inspect(corpus[1])

# use as.character to display the review text or together with writeLines to clean up the display
as.character(cleancorpus[[1]])
writeLines(as.character(corpus[[2]]))

# clean up the data
cleancorpus = tm_map(corpus, content_transformer(tolower))
cleancorpus2 = tm_map(cleancorpus, removePunctuation)
cleancorpus3 = tm_map(cleancorpus2, stripWhitespace)
cleancorpus4 = tm_map(cleancorpus3, removeWords, stopwords("english"))

# create tokenizer functions
unigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 1), paste, collapse = " "), use.names = FALSE)
}

bigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 2), paste, collapse = " "), use.names = FALSE)
}

trigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 3), paste, collapse = " "), use.names = FALSE)
}

# create the document term matrices for unigrams, bigrams, and trigrams
# using this instead of term document matrix allows for cbind back to the review data
# the initial datasets for unigrams, bigrams, and trigrams are stored in a separate Rdata file
unigrams = DocumentTermMatrix(cleancorpus4, control = list(tokenize = unigramTokenizer))
unigrams2 = removeSparseTerms(unigrams, 0.99)
unigramsDF = data.frame(as.matrix(unigrams2))

bigrams = DocumentTermMatrix(cleancorpus4, control = list(tokenize = bigramTokenizer))
bigrams2 = removeSparseTerms(bigrams, 0.99)
bigramsDF = data.frame(as.matrix(bigrams2))

trigrams = DocumentTermMatrix(cleancorpus4, control = list(tokenize = trigramTokenizer)) 
trigrams2 = trigrams[,findFreqTerms(trigrams,lowfreq = 2000)]
trigramsDF = data.frame(as.matrix(trigrams2))

# finding the frequency of words
uni_freq = sort(colSums(as.matrix(unigramsDF)), decreasing=T)
uni_freq = data.frame(word = names(uni_freq), Frequency = uni_freq)

bi_freq = sort(colSums(as.matrix(bigramsDF)), decreasing = T)
bi_freq = data.frame(word = names(bi_freq), Frequency = bi_freq)

tri_freq = colSums(as.matrix(trigrams2))
tri_freq = data.frame(word = names(tri_freq), Frequency = tri_freq)

# creating ngram datasets with star ratings and biz ids
reviewstars = reviews$stars
unigramsdf$reviewstars = reviewstars
bigramsdf$reviewstars = reviewstars
trigramsdf$reviewstars = reviewstars

# adding business stars to the data set
bizstars = biz[,c("stars","business_id")]
bizid = reviews$business_id

unigramsdf$business_id = bizid
bigramsdf$business_id = bizid
trigramsdf$business_id = bizid
unigramsdf = merge(unigramsdf, bizstars, by = "business_id", all.x = T)
bigramsdf = merge(bigramsdf, bizstars, by = "business_id", all.x = T)
trigramsdf = merge(trigramsdf, bizstars, by = "business_id", all.x = T)

#----------#
# Ngrams   #
#----------#

uni.1star = head(sort(colSums(unigramsdf[unigramsdf$`reviews$stars`==1,])),50)
bi.1star = head(sort(colSums(bigramsdf[bigramsdf$`reviews$stars`==1,])),50)
tri.1star = head(sort(colSums(trigramsdf[trigramsdf$`reviews$stars`==1,])),50)

#----------------------------------------------------#
##########
# REVAMP #
##########

# 

# clean up the categories and filter by restaurants
biz$categories = sapply(biz$categories, toString)
biz$categories = sapply(biz$categories, tolower)
restaurants = biz[grep("restaurant",biz$categories),]

# create joint dataset and filter it to just reviews for restaurants
biz.reviews = merge(reviews, restaurants, by="business_id", all.x=T)
biz.reviews = biz.reviews[!is.na(biz.reviews$stars.y),]
quantile(biz.reviews$stars.y, seq(0,1,0.1))
prop.table(table(biz.reviews$stars.y))
table(biz.reviews$stars.y)
summary(biz.reviews$stars.y)

# creating corpuses
corpus.5star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y ==5,]$text))
corpus.4.5star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y ==4.5,]$text))
corpus.1star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y ==1,]$text))
corpus.1.5star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y ==1.5,]$text))
corpus.2star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y ==2,]$text))

# preparing the corpus
corpuscleaner <- function(corpus){
  clean <- tm_map(corpus, content_transformer(tolower))
  clean <- tm_map(clean, removePunctuation)
  clean <- tm_map(clean, stripWhitespace)
  clean <- tm_map(clean, removeWords, stopwords("english"))
  return(clean)
}

# clean the corpuses
corpus.5star <- corpuscleaner(corpus.5star)
corpus.4.5star <- corpuscleaner(corpus.4.5star)
corpus.2star <- corpuscleaner(corpus.2star)
corpus.1star <- corpuscleaner(corpus.1star)
corpus.1.5star <- corpuscleaner(corpus.1.5star)

# create tokenizer functions
unigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 1), paste, collapse = " "), use.names = FALSE)
}

bigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 2), paste, collapse = " "), use.names = FALSE)
}

trigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 3), paste, collapse = " "), use.names = FALSE)
}

# create ngrams 
dtm.1star.uni <- DocumentTermMatrix(corpus.1star, control = list(tokenize = unigramTokenizer))
dtm.1star.bi <- DocumentTermMatrix(corpus.1star, control = list(tokenize = bigramTokenizer))
dtm.1star.tri <- DocumentTermMatrix(corpus.1star, control = list(tokenize = trigramTokenizer))

dtm.1.5star.uni <- DocumentTermMatrix(corpus.1.5star, control = list(tokenize = unigramTokenizer))
dtm.1.5star.bi <- DocumentTermMatrix(corpus.1.5star, control = list(tokenize = bigramTokenizer))
dtm.1.5star.tri <- DocumentTermMatrix(corpus.1.5star, control = list(tokenize = trigramTokenizer))

dtm.2star.uni <- DocumentTermMatrix(corpus.2star, control = list(tokenize = unigramTokenizer))
dtm.2star.bi <- DocumentTermMatrix(corpus.2star, control = list(tokenize = bigramTokenizer))
dtm.2star.tri <- DocumentTermMatrix(corpus.2star, control = list(tokenize = trigramTokenizer))
dtm.2star.bi <- removeSparseTerms(dtm.2star.bi,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.2star.tri <- removeSparseTerms(dtm.2star.tri,sparse=0.99) # remove sparse terms so that the data can be loaded

dtm.4.5star.uni <- DocumentTermMatrix(corpus.4.5star, control = list(tokenize = unigramTokenizer))
dtm.4.5star.bi <- DocumentTermMatrix(corpus.4.5star, control = list(tokenize = bigramTokenizer))
dtm.4.5star.tri <- DocumentTermMatrix(corpus.4.5star, control = list(tokenize = trigramTokenizer))
dtm.4.5star.uni <- removeSparseTerms(dtm.4.5star.uni,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.4.5star.bi <- removeSparseTerms(dtm.4.5star.bi,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.4.5star.tri <- removeSparseTerms(dtm.4.5star.tri,sparse=0.99) # remove sparse terms so that the data can be loaded

dtm.5star.uni <- DocumentTermMatrix(corpus.5star, control = list(tokenize = unigramTokenizer))
dtm.5star.bi <- DocumentTermMatrix(corpus.5star, control = list(tokenize = bigramTokenizer))
dtm.5star.tri <- DocumentTermMatrix(corpus.5star, control = list(tokenize = trigramTokenizer))


# calculating frequencies
freq.1star.uni <- sort(colSums(as.matrix(dtm.1star.uni)),decreasing=T)
freq.1star.bi <- sort(colSums(as.matrix(dtm.1star.bi)),decreasing=T)
freq.1star.tri <- sort(colSums(as.matrix(dtm.1star.tri)),decreasing=T)

freq.1.5star.uni <- sort(colSums(as.matrix(dtm.1.5star.uni)),decreasing=T)
freq.1.5star.bi <- sort(colSums(as.matrix(dtm.1.5star.bi)),decreasing=T)
freq.1.5star.tri <- sort(colSums(as.matrix(dtm.1.5star.tri)),decreasing=T)

freq.2star.uni <- sort(colSums(as.matrix(dtm.2star.uni)),decreasing=T)
freq.2star.bi <- sort(colSums(as.matrix(dtm.2star.bi)),decreasing=T)
freq.2star.tri <- sort(colSums(as.matrix(dtm.2star.tri)),decreasing=T)

freq.4.5star.uni <- sort(colSums(as.matrix(dtm.4.5star.uni)),decreasing=T)
freq.4.5star.bi <- sort(colSums(as.matrix(dtm.4.5star.bi)),decreasing=T)
freq.4.5star.tri <- sort(colSums(as.matrix(dtm.4.5star.tri)),decreasing=T)

freq.5star.uni <- sort(colSums(as.matrix(dtm.5star.uni)),decreasing=T)
freq.5star.bi <- sort(colSums(as.matrix(dtm.5star.bi)),decreasing=T)
freq.5star.tri <- sort(colSums(as.matrix(dtm.5star.tri)),decreasing=T)

# create dataframes from all the frequency matrices
freqdfcreator <- function(uni, bi, tri){
  df.uni <- data.frame(uni)
  names(df.uni) <- "frequency"
  df.uni$ngram <- "unigram"
  df.bi <- data.frame(bi)
  names(df.bi) <- "frequency"
  df.bi$ngram <- "bigram"
  df.tri <- data.frame(tri)
  names(df.tri) <- "frequency"
  df.tri$ngram <- "trigram"
  df <- rbind(df.uni, df.bi, df.tri)
  return(df)
}

df.1star <- freqdfcreator(freq.1star.uni,freq.1star.bi,freq.1star.tri)
df.1.5star <- freqdfcreator(freq.1.5star.uni,freq.1.5star.bi,freq.1.5star.tri)
df.2star <- freqdfcreator(freq.2star.uni,freq.2star.bi,freq.2star.tri)
df.4.5star <- freqdfcreator(freq.4.5star.uni,freq.4.5star.bi,freq.4.5star.tri)
df.5star <- freqdfcreator(freq.1star.uni,freq.1star.bi,freq.1star.tri)

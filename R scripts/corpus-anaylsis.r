###############
# TEXT MINING #
###############

library(dplyr)
library(ggplot2)
library(tm)

# clean up the categories and filter by restaurants
biz$categories = sapply(biz$categories, toString)
biz$categories = sapply(biz$categories, tolower)
restaurants = biz[grep("restaurant",biz$categories),]

# create joint dataset and filter it to just reviews for restaurants
biz.reviews = merge(reviews, restaurants, by="business_id", all.x=T)
biz.reviews = biz.reviews[!is.na(biz.reviews$stars.y),]
quantile(biz.reviews$stars.y, seq(0,1,0.1))
prop.table(table(biz.reviews$stars.y))
table(biz.reviews$stars.y)
summary(biz.reviews$stars.y)

# subset by 1, 1.5, 2, 2.5, 4.5, and 5 star businesses
biz.reviews.subset <- biz.reviews[biz.reviews$stars.y==1 | biz.reviews$stars.y==1.5 |
                                    biz.reviews$stars.y==2 |biz.reviews$stars.y==2.5 |
                                    biz.reviews$stars.y==4.5 | biz.reviews$stars.y==5,]

biz.reviews.subset$stars.classify <- biz.reviews.subset$stars.y
biz.reviews.subset[biz.reviews.subset$stars.classify==1.5,]$stars.classify <- 1
biz.reviews.subset[biz.reviews.subset$stars.classify==2.5,]$stars.classify <- 2
biz.reviews.subset[biz.reviews.subset$stars.classify==4.5,]$stars.classify <- 5

# creating corpuses
corpus.1star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y ==1 | biz.reviews$stars.y==1.5,]$text))
corpus.2star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y ==2 | biz.reviews$stars.y==2.5,]$text))
corpus.5star <- Corpus(VectorSource(biz.reviews[biz.reviews$stars.y==5 | biz.reviews$stars.y==4.5,]$text))
all.corpus <- Corpus(VectorSource(biz.reviews.subset$text))

# preparing the corpus
corpuscleaner <- function(corpus){
  clean <- tm_map(corpus, content_transformer(tolower))
  clean <- tm_map(clean, removePunctuation)
  clean <- tm_map(clean, stripWhitespace)
  clean <- tm_map(clean, removeWords, stopwords("english"))
  return(clean)
}

# clean the corpuses
corpus.1star <- corpuscleaner(corpus.1star)
corpus.2star <- corpuscleaner(corpus.2star)
corpus.5star <- corpuscleaner(corpus.5star)
all.corpus <- corpuscleaner(all.corpus)

# create tokenizer functions
unigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 1), paste, collapse = " "), use.names = FALSE)
}

bigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 2), paste, collapse = " "), use.names = FALSE)
}

trigramTokenizer = function(x){
  unlist(lapply(ngrams(words(x), 3), paste, collapse = " "), use.names = FALSE)
}

# create ngrams 
dtm.1star.uni <- DocumentTermMatrix(corpus.1star, control = list(tokenize = unigramTokenizer))
dtm.1star.bi <- DocumentTermMatrix(corpus.1star, control = list(tokenize = bigramTokenizer))
dtm.1star.tri <- DocumentTermMatrix(corpus.1star, control = list(tokenize = trigramTokenizer))
dtm.1star.uni <- removeSparseTerms(dtm.1star.uni,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.1star.bi <- removeSparseTerms(dtm.1star.bi,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.1star.tri <- dtm.1star.tri[,findFreqTerms(dtm.1star.tri,lowfreq=10)] # remove sparse terms so that the data can be loaded

dtm.2star.uni <- DocumentTermMatrix(corpus.2star, control = list(tokenize = unigramTokenizer))
dtm.2star.bi <- DocumentTermMatrix(corpus.2star, control = list(tokenize = bigramTokenizer))
dtm.2star.tri <- DocumentTermMatrix(corpus.2star, control = list(tokenize = trigramTokenizer))
dtm.2star.uni <- removeSparseTerms(dtm.2star.uni,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.2star.bi <- removeSparseTerms(dtm.2star.bi,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.2star.tri <- dtm.2star.tri[,findFreqTerms(dtm.2star.tri,lowfreq=100)] # remove terms with less than 100 frequency

dtm.5star.uni <- DocumentTermMatrix(corpus.5star, control = list(tokenize = unigramTokenizer))
dtm.5star.bi <- DocumentTermMatrix(corpus.5star, control = list(tokenize = bigramTokenizer))
dtm.5star.tri <- DocumentTermMatrix(corpus.5star, control = list(tokenize = trigramTokenizer))
dtm.5star.uni <- removeSparseTerms(dtm.5star.uni,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.5star.bi <- removeSparseTerms(dtm.5star.bi,sparse=0.99) # remove sparse terms so that the data can be loaded
dtm.5star.tri <- dtm.5star.tri[,findFreqTerms(dtm.5star.tri,lowfreq=100)] # remove terms with less than 100 frequency

all.dtm.uni <- DocumentTermMatrix(all.corpus, control = list(tokenize=unigramTokenizer))
all.dtm.bi <- DocumentTermMatrix(all.corpus, control = list(tokenize=bigramTokenizer))
all.dtm.tri <- DocumentTermMatrix(all.corpus, control = list(tokenize=trigramTokenizer))
all.dtm.uni <- removeSparseTerms(all.dtm.uni,sparse=0.95)
all.dtm.bi <- removeSparseTerms(all.dtm.bi,sparse=0.99)
all.dtm.tri <- all.dtm.tri[,findFreqTerms(all.dtm.tri, lowfreq=200)]

# calculating frequencies
freq.1star.uni <- sort(colSums(as.matrix(dtm.1star.uni)),decreasing=T)
freq.1star.bi <- sort(colSums(as.matrix(dtm.1star.bi)),decreasing=T)
freq.1star.tri <- sort(colSums(as.matrix(dtm.1star.tri)),decreasing=T)

freq.2star.uni <- sort(colSums(as.matrix(dtm.2star.uni)),decreasing=T)
freq.2star.bi <- sort(colSums(as.matrix(dtm.2star.bi)),decreasing=T)
freq.2star.tri <- sort(colSums(as.matrix(dtm.2star.tri)),decreasing=T)

freq.5star.uni <- sort(colSums(as.matrix(dtm.5star.uni)),decreasing=T)
freq.5star.bi <- sort(colSums(as.matrix(dtm.5star.bi)),decreasing=T)
freq.5star.tri <- sort(colSums(as.matrix(dtm.5star.tri)),decreasing=T)

all.freq.uni <- sort(colSums(as.matrix(all.dtm.uni)),decreasing=T)
all.freq.bi <- sort(colSums(as.matrix(all.dtm.bi)),decreasing=T)
all.freq.tri <- sort(colSums(as.matrix(all.dtm.tri)),decreasing=T)

# create plots of top terms
freqdf.1star.tri <- head(data.frame(words=names(freq.1star.tri), frequency=freq.1star.tri, row.names=NULL),30)
freqdf.2star.tri <- head(data.frame(words=names(freq.2star.tri), frequency=freq.2star.tri, row.names=NULL),30)
freqdf.5star.tri <- head(data.frame(words=names(freq.5star.tri), frequency=freq.5star.tri, row.names=NULL),30)

freqdf.1star.tri$words <-reorder(freqdf.1star.tri$words, freqdf.1star.tri$frequency)
freqdf.2star.tri$words <-reorder(freqdf.2star.tri$words, freqdf.2star.tri$frequency)
freqdf.5star.tri$words <-reorder(freqdf.5star.tri$words, freqdf.5star.tri$frequency)

plot.1star.tri <- ggplot(freqdf.1star.tri, aes(words,frequency)) + geom_bar(stat="identity", fill="red") + 
  ggtitle("1 star trigrams") + coord_flip()
plot.2star.tri <- ggplot(freqdf.2star.tri, aes(words,frequency)) + geom_bar(stat="identity", fill="orange") + 
  ggtitle("2 star trigrams") + coord_flip()
plot.5star.tri <- ggplot(freqdf.5star.tri, aes(words,frequency)) + geom_bar(stat="identity", fill="green") +
  ggtitle("5 star trigrams") + coord_flip()
grid.arrange(plot.1star.tri,plot.2star.tri,plot.5star.tri,ncol=2,nrow=2)

# prediction
library(doParallel)
library(caret)
set.seed(2102)

# combine the separate ngrams into a single dataframe
df <- data.frame(as.matrix(cbind(biz.reviews.subset$stars.rating, all.dtm.uni, all.dtm.bi, all.dtm.tri)))
names(df)[names(df)=="V1"] <- "star.rating"
df$star.rating <- factor(df$star.rating)

# create training and test sets; 10% of the original data is used to speed up model creation
df_partition <- createDataPartition(df$star.rating, p=0.1, list=F)
df_partition <- df[df_partition,]
df_trainrandom <- createDataPartition(df_partition$star.rating, p=0.7, list=F)
df_train <- df_partition[df_trainrandom,]
df_test <- df_partition[-df_trainrandom,]

# create clusters
cluster <- makeCluster(detectCores() - 1)
registerDoParallel(cluster)

# train the models
model.rpart <- train(star.rating ~., data=df_train, method="rpart")
model.nnet <- train(star.rating ~., data=df_train, method="nnet")
model.logitboost <- train(star.rating~., data=df_train, method="LogitBoost")

# predict
predict.rpart <- predict(model.rpart, df_test)
predict.nnet <- predict(model.nnet, df_test)
predict.logitboost <- predict(model.logitboost,df_test)

# generate results
results.rpart <- confusionMatrix(df_test$star.rating,predict.rpart)
results.nnet <- confusionMatrix(df_test$star.rating,predict.nnet)
results.logitboost <- confusionMatrix(df_test$star.rating,predict.logitboost)

##################
# UNUSED SCRIPTS #
##################

# create dataframes from all the frequency matrices
freqdfcreator <- function(uni, bi, tri){
  df.uni <- data.frame(uni)
  names(df.uni) <- "frequency"
  df.uni$ngram <- "unigram"
  df.bi <- data.frame(bi)
  names(df.bi) <- "frequency"
  df.bi$ngram <- "bigram"
  df.tri <- data.frame(tri)
  names(df.tri) <- "frequency"
  df.tri$ngram <- "trigram"
  df <- rbind(df.uni, df.bi, df.tri)
  return(df)
}

freqdf.1star <- freqdfcreator(freq.1star.uni,freq.1star.bi,freq.1star.tri)
freqdf.1star <- freqdf.1star[order(-freqdf.1star$frequency),]

freqdf.2star <- freqdfcreator(freq.2star.uni,freq.2star.bi,freq.2star.tri)
freqdf.2star <- freqdf.2star[order(-freqdf.2star$frequency),]

freqdf.5star <- freqdfcreator(freq.5star.uni,freq.5star.bi,freq.5star.tri)
freqdf.5star <- freqdf.5star[order(-freqdf.5star$frequency),]

# LOADING DATA

# Loading libraries
library(jsonlite)
library(tm)

# use stream_in method from jsonlite as the dataset is not properly formed json
business_raw = stream_in(file("yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_business.json"))
checkin_raw = stream_in(file("yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_checkin.json"))
review_raw = stream_in(file("yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_review.json"))
tip_raw = stream_in(file("yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_tip.json"))
user_raw = stream_in(file("yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_user.json"))

#flatten all the nested dataframes
business_raw = flatten(business_raw)
checkin_raw = flatten(checkin_raw)
review_raw = flatten(review_raw)
tip_raw = flatten(tip_raw)
user_raw = flatten(user_raw)

# create a corpus from the review texts for text mining
review_corpus = Corpus(VectorSource(review_raw$text))

save.image("~/coursera-capstone/raw_data.RData")

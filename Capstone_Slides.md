Predicting Business Yelp Ratings through the Text Analysis of Reviews
========================================================
author: Lucas Neo
date: 22nd November 2015



Introduction
========================================================
Yelp is an online platform which crowd-sources for ratings and reviews on businesses. In this highly interconnected era, the social validation businesses receive through these platforms are becoming increasingly important.

The main goal of analysing this dataset are:  

1. To understand the differences in review words used for businesses with 1 star, 2 star, and 5 star ratings  
2. Predict the business rating of a restaurant based on the words used in reviews of their business

Methods
========================================================
The `tm` package was used to cast the review text into a corpus. Subsequently, a document term matrix was created from the corpus.

The exploratory analysis split the dataset by star rating and found that there was indeed a difference in the words used.

Lastly, 3 models were trained on the dataset; `rpart`, `nnet`, and `logitboost`. The trained models were then used to predict the classification of a business by running each model on the test dataset.

Results
========================================================
Results of `rpart`:

```
 Accuracy 
0.7339519 
```
Results of `nnet`:

```
 Accuracy 
0.7754213 
```
Results of `logitboost`:

```
 Accuracy 
0.7353124 
```

Summary
========================================================
Of the 3 models used, neural networks had the best overall accuracy.

However, an inspection of the results by class revealed that the predictions for 1 star ratings were not very robust.

To improve on this, more data for 1 and 1.5 star businesses is required.

Nevertheless, the `nnet` model is can still be employed by Yelp businesses to predict their Yelp business rating.
